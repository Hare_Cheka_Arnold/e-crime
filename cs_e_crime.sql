-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2015 at 01:09 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cs_e_crime`
--
CREATE DATABASE IF NOT EXISTS `cs_e_crime` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cs_e_crime`;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

CREATE TABLE IF NOT EXISTS `audit` (
  `action_id` int(50) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) NOT NULL,
  `time` varchar(100) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `host_comp` text NOT NULL,
  PRIMARY KEY (`action_id`),
  KEY `actor_id` (`actor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_category`
--

CREATE TABLE IF NOT EXISTS `ecrime_category` (
  `Category_Name` varchar(200) NOT NULL COMMENT 'Name of the crime category',
  `Category_Description` text NOT NULL COMMENT 'Crime Description',
  `Category_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Crime ID',
  PRIMARY KEY (`Category_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_crime_registration`
--

CREATE TABLE IF NOT EXISTS `ecrime_crime_registration` (
  `Crime_Registration_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Crime_Number` varchar(200) NOT NULL,
  `Crime_Category_ID` int(11) NOT NULL,
  `Crime_Descrition` text NOT NULL,
  `Accused_Name` varchar(200) DEFAULT 'None',
  `Complainant_Name` varchar(200) DEFAULT 'None',
  `Report_Date` text NOT NULL,
  `Crime_Status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0=Not Solved, 1= Solved',
  `Crime_Officer` varchar(200) NOT NULL COMMENT 'officer name',
  PRIMARY KEY (`Crime_Registration_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_department`
--

CREATE TABLE IF NOT EXISTS `ecrime_department` (
  `Department_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Department_Name` varchar(200) NOT NULL,
  `Faculty_ID` varchar(6) NOT NULL,
  PRIMARY KEY (`Department_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_discplinary`
--

CREATE TABLE IF NOT EXISTS `ecrime_discplinary` (
  `Discplinary_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Crime_Officer_Conc` text,
  `Crime_Rule` text,
  `Discplinary_Date` varchar(200) DEFAULT NULL,
  `Crime_No` int(11) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Witness1` varchar(200) DEFAULT NULL,
  `Witness2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Discplinary_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_faculty`
--

CREATE TABLE IF NOT EXISTS `ecrime_faculty` (
  `Faculty_Name` varchar(200) NOT NULL,
  `Faculty_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Faculty_Description` text NOT NULL,
  PRIMARY KEY (`Faculty_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_officers_details`
--

CREATE TABLE IF NOT EXISTS `ecrime_officers_details` (
  `Officerme_Cri_ID` mediumint(11) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(200) NOT NULL,
  `Officer_Status` tinyint(3) NOT NULL COMMENT '0=free, 1=Assigned, 2=Suspended, 4=Delete',
  `Officer_ID` varchar(11) NOT NULL,
  `Officer_Tel` varchar(200) NOT NULL,
  `Crime_Closure_free` varchar(200) DEFAULT NULL,
  `userid_users` int(200) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Officerme_Cri_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecrime_user_registration`
--

CREATE TABLE IF NOT EXISTS `ecrime_user_registration` (
  `Full_Name` varchar(200) NOT NULL,
  `ID_Number` varchar(200) NOT NULL,
  `Tel_NO` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Reg_Date` varchar(200) NOT NULL,
  `Reg_NO` varchar(200) NOT NULL,
  `Title_No` varchar(200) NOT NULL,
  `Faculty` varchar(3) NOT NULL,
  `Department` varchar(3) NOT NULL,
  `Course_Year` text NOT NULL,
  `Status` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Reg_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stop_login_date`
--

CREATE TABLE IF NOT EXISTS `stop_login_date` (
  `Date_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Actual_Date` varchar(200) NOT NULL,
  `Determiner` varchar(200) NOT NULL,
  PRIMARY KEY (`Date_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stop_login_date`
--

INSERT INTO `stop_login_date` (`Date_ID`, `Actual_Date`, `Determiner`) VALUES
(1, 'Aug-30-2015 06:00:10 AM', 'ed3b0f1e00a67acb17c637545322056b');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `mname`, `lname`, `phone`, `username`, `password`, `level`, `timestamp`) VALUES
(384749, 'Developer Hare Cheka Arnold', NULL, NULL, '0717212724', 'Cheka', 'ed3b0f1e00a67acb17c637545322056b', '10', '2015-08-22 03:25:28'),
(3847491, 'Admin Anne Waithira', NULL, NULL, '0702212525', 'Anne', '1552c03e78d38d5005d4ce7b8018addf', '4', '2015-03-28 09:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE IF NOT EXISTS `years` (
  `Year_name` varchar(200) NOT NULL,
  `Year_ID` int(3) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Year_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
