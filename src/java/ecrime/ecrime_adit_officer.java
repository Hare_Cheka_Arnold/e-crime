/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_adit_officer extends HttpServlet {

    //INitialising variables
    String fullname,idno,telno,officer_status,officer_registration_ID;
    
    ///Initialising session
     HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        //Getting session
        session = request.getSession();
        
        //Creating data object
        ecrime_source conn = new ecrime_source();
        
        /*Receiving parameters of the passed form*/
        fullname = request.getParameter("full_name");
        idno = request.getParameter("officerID");
        telno = request.getParameter("phone_number");
        officer_status = request.getParameter("officer_Status");
        officer_registration_ID = request.getParameter("userid");
        
        //Writing a Query to get the entire details
        String save_Offe_detail = "UPDATE ecrime_officers_details SET FullName ='"+fullname+"',Officer_ID = '"+idno+"',"
                + "Officer_Tel = '"+telno+"',Officer_Status = '"+officer_status+"' WHERE "
                + "Officerme_Cri_ID = '"+officer_registration_ID+"'";
        
        //Executig the QUery
        conn.st1.executeUpdate(save_Offe_detail);
        
        //Creating the session
            session.setAttribute("Man_Offic_edited", "<font color=\"green\">Officer "+fullname+" and ID Number "+idno+" Edited Succesfully</font>");
       
            
            //Redirecting to another page
            response.sendRedirect("E-Crime_Man_Officers.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_adit_officer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_adit_officer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
