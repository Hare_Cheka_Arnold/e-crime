/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_discplinary_ruling extends HttpServlet {

    //INitialising variables
    String Crime_NO,Discplinary_ID,Crime_Description;
    
    //Initialising sesion
     HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        //*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Crime_NO = request.getParameter("Crime_Number");
            Discplinary_ID = request.getParameter("Crime_Reg_ID");
            Crime_Description = request.getParameter("Crime_Desc");
            
            
            //Assingning them to session
            session.setAttribute("Discplinay_CrimeNo", Crime_NO);
            session.setAttribute("Discplinay_DiscplinaryID", Discplinary_ID);
            session.setAttribute("Discplinay_CrimeDescription", Crime_Description);
            
            response.sendRedirect("E-Crime_Discplinary_Ruling_View.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
