/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_add_officers extends HttpServlet {

    /*Initialising the Variables*/
    HttpSession session;
    
    String Officer_Names,Officer_ID,Officer_Tel,userid;
    MessageDigest m;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, SQLException {
        
        //Getting session
        session=request.getSession();
        
        //Initialising the db connection
        ecrime_source conn = new ecrime_source();
        
        //Receiving parameters from form
        Officer_Names = request.getParameter("full_name");
        Officer_Tel = request.getParameter("phone_number");
        Officer_ID = request.getParameter("officerID");
         userid = request.getParameter("userid");
         
         String Bothnames = "Officer "+Officer_Names;
         
         int officeruserid = generateRandomNumber(3941,1870888);
        
        //Encrypting password
        m = MessageDigest.getInstance("MD5");
        m.update(Officer_ID.getBytes(), 0, Officer_ID.length());
        String pw = new BigInteger(1, m.digest()).toString(16);
        
        //Initialising the Queries
        String save_officer_details = "insert into ecrime_officers_details (FullName,Officer_ID,Officer_Tel,Officer_Status,"
                + "userid_users)"
               + "values ('" + Officer_Names + "','" + Officer_ID + "','" + Officer_Tel + "','" + 0 + "','"+officeruserid+"'"
                + ")";
        
        String Save_officer_user = "INSERT INTO users (user_id,fname,phone,username,password,level)VALUES('"+officeruserid+"',"
                + "'"+Bothnames+"','"+Officer_Tel+"','"+Officer_Tel+"','"+pw+"','"+1+"')";
        
        //Executing the Queries
        conn.st1.executeUpdate(save_officer_details);
        
        conn.st1.executeUpdate(Save_officer_user);
        
        //Setting the session to show everything was successfull
        session.setAttribute("add_officer", "<font color=\"green\">E-Crime Officer "+Officer_Names+" added succesfully</font>");
        
        //Redirecting to the page
        response.sendRedirect("E-Crime_Add_Detectives.jsp");
        
            
                   
                   
                   
    }
     public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(ecrime_add_officers.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ecrime_add_officers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (SQLException ex) {
                Logger.getLogger(ecrime_add_officers.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ecrime_add_officers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
