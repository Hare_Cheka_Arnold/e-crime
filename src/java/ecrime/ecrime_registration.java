/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anne Waithira
 */
public class ecrime_registration extends HttpServlet {

    //Initialising of variables
    HttpSession session;
    String crimecategory,crimedesc,AccusedName,reportdate,complainantname,Assigned_officer,AccusedNameDet;
      String Crime_No,ComplainantNameDet;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        //Getting session
        session=request.getSession();
        
         //Creates an object for db connection
        ecrime_source conn = new ecrime_source();
        
        //Receiving Parameters from the registration form
        crimecategory = request.getParameter("crimecategory");
        crimedesc = request.getParameter("crimedesc");
        AccusedName = request.getParameter("AccusedName");
        if (AccusedName.isEmpty()){
            AccusedNameDet = "NONE";        
        }else{
            AccusedNameDet = request.getParameter("AccusedName");        
        }
        complainantname = request.getParameter("complainantname");
        if (complainantname.isEmpty()){
            ComplainantNameDet = "NONE";
            }else{
        ComplainantNameDet = request.getParameter("complainantname");
        }
        reportdate = request.getParameter("reportdate");
        Assigned_officer = request.getParameter("Assigned_officer");
        Crime_No = request.getParameter("CrimeNo");
        
        //Initialising Queries
        String Register_Crime = "INSERT INTO ecrime_crime_registration (Crime_Category_ID,Crime_Descrition,Accused_Name"
                + ",Complainant_Name,Report_Date,Crime_Status,Crime_Officer,Crime_Number) VALUES('"+crimecategory+"','"+crimedesc+"',"
                + "'"+AccusedNameDet+"','"+ComplainantNameDet+"','"+reportdate+"','"+0+"','"+Assigned_officer+"','"+Crime_No+"')";
        
        //Updating Crime officer status
        String Update_off_status = "UPDATE ecrime_officers_details SET Officer_Status = '"+1+"' WHERE "
                + "Officerme_Cri_ID = '"+Assigned_officer+"'";
        
        //Executing the Query
        conn.st.executeUpdate(Register_Crime);
         conn.st1.executeUpdate(Update_off_status);
        
        //Creating session
         session.setAttribute("Crime_registration", "<font color=\"green\">Crime wid description "+crimedesc+" Registered succesfully</font>");
         
         //Redirects the system the registration page
        response.sendRedirect("E-Crime_Registration.jsp");
               
        
        
       
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_registration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_registration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
