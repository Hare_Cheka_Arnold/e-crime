/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_add_year extends HttpServlet {

   
    //Initialising the variables
   
    HttpSession session;
    String year, County_Description;
      boolean statuz = false;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            year = request.getParameter("year");
            
            
            
            //Creating a new object for the data source
            ecrime_source conn = new ecrime_source();
            
            //Initialising the query to insert new counties.                      

            String save_county = "INSERT INTO years (Year_name) "
                    + "VALUES ('" + year + "')";
            
           
            
            //Initialising the query to check if the county exists

            String county_checker = "SELECT * FROM years WHERE Year_name='" + year + "'";


            //Statement to execute the query to check
            conn.rs = conn.st.executeQuery(county_checker);

            //check if ward is already registered 
            if (!conn.rs.next()) {
                
                conn.st1.executeUpdate(save_county);
               
                
                                  

                session.setAttribute("year_added", "<font color=\"green\">Year "+year+" added succesfully</font>");
            } else {
                session.setAttribute("year_added", "<b><font color=\"red\">Sorry, Year "+year+" is already registered.</font></b>");


            }
            
            response.sendRedirect("E-Crime_Add_Year.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(ecrime_add_year.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
