/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;


import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anne Waithira
 */
public class ecrime_edit_profile extends HttpServlet {

   String fname,idno,phone,email,password,userid,username,userlevel,Both_Names;
int counter;
    MessageDigest m;
HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        try {
            ecrime_source conn = new ecrime_source();
            counter=0;
            session=request.getSession();
            
           userid=request.getParameter("userid");
           fname=request.getParameter("full_name");
           userlevel=request.getParameter("userlevel");
           username=request.getParameter("username");
           phone=request.getParameter("phone_number");
           password=request.getParameter("pass");
           
           
            //encrypt password
               
                   m = MessageDigest.getInstance("MD5");
                   m.update(password.getBytes(), 0, password.length());
                   String pw = new BigInteger(1, m.digest()).toString(16);
           
           
           
           if(userid!=null && !userid.equals("")){
               
              String check_userid_existence="SELECT COUNT(user_id) FROM users WHERE user_id='"+userid+"'";
              conn.rs=conn.st.executeQuery(check_userid_existence);
              if(conn.rs.next()==true){
                  counter=conn.rs.getInt(1);
                 }
               
               if(counter>0){
                  String updator="UPDATE users SET fname='"+fname+"',phone='"+phone+"',username='"+username+"',password='"+pw+"' WHERE user_id='"+userid+"'";
                   
                   int update= conn.st.executeUpdate(updator);
            
            
         
            if(update>0){
                
            session.setAttribute("user_updated", "<font color=\"green\">Users Details Updated succesfully</font>");
             
             }
            else {
                session.setAttribute("user_updated", "<b><font color=\"red\">Sorry, Users Details Did not Update Successful</font></b>");
            }
            
               }
               
           }
    conn.rs.close();
    conn.st.close();
    
    //Redirecting to the profile editing page
           response.sendRedirect("E-Crime_Profile_Editing.jsp");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ecrime_edit_profile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_edit_profile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_edit_profile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
