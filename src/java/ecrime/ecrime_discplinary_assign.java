/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_discplinary_assign extends HttpServlet {

    //Initialising Variables
    String CrimeNo,CrimeCategory,crimedesc,AccusedName,complainantname,reportdate,assigned_off,crime_status,discp_date;
    
    //INitialisition of the Session
    private HttpSession session;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        //Getting session
        session = request.getSession();
        
        //Creating data object
        ecrime_source conn = new ecrime_source();
        
        /*Receiving parameters of the passed form*/
        CrimeNo = request.getParameter("CrimeNo");
        CrimeCategory = request.getParameter("CrimeCategory");
        crimedesc = request.getParameter("crimedesc");
        AccusedName = request.getParameter("AccusedName");
        complainantname = request.getParameter("complainantname");
        reportdate = request.getParameter("reportdate");
        assigned_off = request.getParameter("assigned_off");
        crime_status = request.getParameter("crime_status");
        discp_date = request.getParameter("discp_date");
        
        //Executing the Queries
        String Assign_date = "INSERT INTO ecrime_discplinary (Crime_No, Discplinary_Date) VALUES ('" + CrimeNo + "',"
                + "'" + discp_date + "')";
        
        //Executing the query
        conn.st1.executeUpdate(Assign_date);
        
        //Setting the session to show everything was successfull
        session.setAttribute("Man_Crime_edited", "<font color=\"green\">Discplinary Date of Crime Number <b>"+CrimeNo+"</b> was succesfully</font>");
        
        //Redirecting to the page
        response.sendRedirect("E-Crime_Man_Crimes.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_discplinary_assign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_discplinary_assign.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
