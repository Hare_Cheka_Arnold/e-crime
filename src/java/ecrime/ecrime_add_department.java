/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_add_department extends HttpServlet {

     //Initialising the variables
   
    HttpSession session;
    String Department_Name,Faculty;
      boolean statuz = false;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Department_Name = request.getParameter("depart_name");
           
            Faculty = request.getParameter("Faculty");
            
            //Creating a new object for the data source
            ecrime_source conn = new ecrime_source();
            
            //Initialising the query to insert new users.                      

            String save_dept = "INSERT INTO ecrime_department (Department_Name,Faculty_ID) "
                    + "VALUES ('" + Department_Name + "','" + Faculty + "')";
            
            //Initialising the query to check if the user exists

            String Dept_checker = "SELECT * FROM ecrime_department WHERE Department_Name='" + Department_Name + "'";


            //Statement to execute the query to check
            conn.rs = conn.st.executeQuery(Dept_checker);

            //check if ward is already registered 
            if (!conn.rs.next()) {
                
                //if ward does not exist execute the query to insert new ward
                conn.st1.executeUpdate(save_dept);

                

                session.setAttribute("Department_added", "<font color=\"green\">Department "+Department_Name+" added succesfully</font>");
            } else {
                session.setAttribute("Department_added", "<b><font color=\"red\">Sorry, Department "+Department_Name+" is already registered.</font></b>");


            }
            
            response.sendRedirect("E-Crime_Add_Department.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(ecrime_add_department.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
