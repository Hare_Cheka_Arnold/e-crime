/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_ruling_defined extends HttpServlet {

    //INitialising variables
    String Crime_NO,Discplinary_ID,Crime_Description,Panel_Ruling;
    
    //Initialising sesion
     HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
   //*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Crime_NO = request.getParameter("category_name");
            Discplinary_ID = request.getParameter("disc_id");
            Crime_Description = request.getParameter("category_des");
            Panel_Ruling = request.getParameter("panel_des");
            
            //Creating an object
            ecrime_source conn = new ecrime_source();
            
            //Initialising Queries
            String Save_Ruling = "Update ecrime_discplinary SET Crime_Rule ='"+Panel_Ruling+"' WHERE Crime_No ='"+Crime_NO+"'";
            
            String Change_Crime_Status = "Update ecrime_crime_registration SET Crime_Status = '"+1+"' WHERE Crime_Number"
            + "= '"+Crime_NO+"'";
            
            //Execute the Queries
            conn.st1.executeUpdate(Save_Ruling);
            
            conn.st2.executeUpdate(Change_Crime_Status);
            
            session.setAttribute("DiscView_Crime_edited", "<font color=\"green\">Panel Ruling of Crime Number "+Crime_NO+" saved succesfully</font>");
            
            ///Redirecting to Discplinary View Page
            response.sendRedirect("E-Crime_Discplinary_View.jsp");
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_ruling_defined.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_ruling_defined.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
