/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;


import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Anne Waithira
 */
public class ecrime_login extends HttpServlet {

    String uname,pass,error_login,nextPage,current_time;
    String computername;
    MessageDigest m;
    String full_name;
    HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, SQLException {


        ecrime_source conn = new ecrime_source();

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        String yr, mnth, dater, hr, mn, sc, action = "";
        yr = Integer.toString(year);
        mnth = Integer.toString(month);
        dater = Integer.toString(date);
        hr = Integer.toString(hour);
        mn = Integer.toString(min);
        sc = Integer.toString(sec);
        session = request.getSession();

//____________________COMPUTER NAME____________________________________
        computername = InetAddress.getLocalHost().getHostName();
        System.out.println("Computer name " + computername);
        session.setAttribute("computer_name", computername);





//current_time=sc+"-"+mn+"-"+hr+"-"+dater+"-"+mnth+"-"+yr;

//current_time=yr+"-"+mnth+"-"+dater+"-"+hr+":"+mn+":"+sc;

        current_time = yr + "-" + mnth + "-" + dater + "-" + hr + ":" + mn + ":" + sc;
        //get username and password

        uname = request.getParameter("uname");
        //System.out.println("user name " + uname);

        pass = request.getParameter("pass");
        //System.out.println("Paasword " + pass);






        //encrypt password

        m = MessageDigest.getInstance("MD5");
        m.update(pass.getBytes(), 0, pass.length());
        String pw = new BigInteger(1, m.digest()).toString(16);


        //connection to database class instance


        //query for checking user existance in the database
        String select1 = "select * from users";
        






        conn.rs = conn.st.executeQuery(select1);
        System.out.println("Query " + select1);

         System.out.println("username:"+uname+"  Password :"+pw );


        while (conn.rs.next()) {
            if (conn.rs.getString("username").equals(uname) && conn.rs.getString("password").equals(pw)) {

                error_login = null;
                if (conn.rs.getString("level").equals("3")) {
                    String ip = InetAddress.getLocalHost().getHostAddress();
                      System.out.println("level:"+conn.rs.getString("level"));
                    String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in ',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";

                    //String inserter="insert into audit  (action,time,actor_id,host_comp) values ('"++"','"+"')";

                    conn.st3.executeUpdate(inserter);
                    //Initiating query to be executed
                    String dater_invention = "";
                  String get_stop_login = "SELECT * FROM stop_login_date WHERE Date_ID = '"+1+"'";  
                    //If the query execution yields
                   conn.rs5 = conn.st5.executeQuery(get_stop_login);
                   while (conn.rs5.next()) {
                   dater_invention = conn.rs5.getString("Actual_Date");
                   }
                    //print to console
                   System.out.println("This is the function that checks = "+isExpire(dater_invention));
                    //the next page to be opened based on user level
                    if (isExpire(dater_invention) == true){
                        error_login = "<b><font color=\"red\">"+uname+", You need Authorization From Hare Cheka Arnold</font></b>";
                        nextPage = "index.jsp";
                        session.setAttribute("username", conn.rs.getString("username"));
                
                }
                    else{
                    
                    nextPage = "E-Crime_Home.jsp";
                     //save current user details into a session

                   
                    session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("mname"));
                    session.setAttribute("level", conn.rs.getString("level"));
                    session.setAttribute("userid", conn.rs.getString("user_id"));
                    session.setAttribute("username", conn.rs.getString("username"));
                    String fulname = session.getAttribute("fname").toString();
                    session.setAttribute("fullname", fulname);
                   

                    System.out.println( session.getAttribute("fname")+" __"+session.getAttribute("lname"));
                    
                    //get teacher details from the teacher registration table 


                    /** code for auditing  */
                    // conn.st.executeUpdate(audit);
                    
}
                    break;
                }//end of Speaker level
//*****************************************************Start of Administrator Cheka*************************
  if (conn.rs.getString("level").equals("10")) {
                    String ip = InetAddress.getLocalHost().getHostAddress();
                      System.out.println("level:"+conn.rs.getString("level"));
                    String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in ',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";

                    //String inserter="insert into audit  (action,time,actor_id,host_comp) values ('"++"','"+"')";

                    conn.st3.executeUpdate(inserter);
                   nextPage = "E-Crime_Home.jsp";


//String fulname=""+conn.rs.getString("firstname") + " "+conn.rs.getString("lastname");
//audit="Insert into audit (Action,User) values('Logged in','"+fulname+"')";



                    //save current user details into a session

                   
                    session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("mname"));
                    session.setAttribute("level", conn.rs.getString("level"));
                    session.setAttribute("userid", conn.rs.getString("user_id"));
                    session.setAttribute("username", conn.rs.getString("username"));
                    String fulname = session.getAttribute("fname").toString();
                    session.setAttribute("fullname", fulname);
                   

                    //System.out.println( session.getAttribute("fname")+" __"+session.getAttribute("lname"));
                    
                    //get teacher details from the teacher registration table 


                    /** code for auditing  */
                    // conn.st.executeUpdate(audit);
                    break;
                }
 //*********************************************************End of Administrator Cheka**********************
                if (conn.rs.getString("level").equals("1")) {
                    String ip = InetAddress.getLocalHost().getHostAddress();
                      System.out.println("level:"+conn.rs.getString("level"));
                    String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in ',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";

                    //String inserter="insert into audit  (action,time,actor_id,host_comp) values ('"++"','"+"')";

                    conn.st3.executeUpdate(inserter);
                    //Initiating query to be executed
                    String dater_invention = "";
                  String get_stop_login = "SELECT * FROM stop_login_date WHERE Date_ID = '"+1+"'";  
                    //If the query execution yields
                   conn.rs5 = conn.st5.executeQuery(get_stop_login);
                   while (conn.rs5.next()) {
                   dater_invention = conn.rs5.getString("Actual_Date");
                   }
                    //print to console
                   System.out.println("This is the function that checks = "+isExpire(dater_invention));
                    //the next page to be opened based on user level
                    if (isExpire(dater_invention) == true || dater_invention.isEmpty()){
                        error_login = "<b><font color=\"red\">"+uname+", You need Authorization From Hare Cheka Arnold</font></b>";
                        nextPage = "index.jsp";
                        session.setAttribute("username", conn.rs.getString("username"));
                
                }
                    else{
                    
                    nextPage = "E-Crime_Home.jsp";
                    //save current user details into a session

                   
                    session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("mname"));
                    session.setAttribute("level", conn.rs.getString("level"));
                    session.setAttribute("userid", conn.rs.getString("user_id"));
                    session.setAttribute("username", conn.rs.getString("username"));
                    String fulname = session.getAttribute("fname").toString();
                    session.setAttribute("fullname", fulname);
                   
}

break;
                }
                //****************************Clerk module**********************************************        
                else if (conn.rs.getString("level").equals("4")) {
                    //Initiating query to be executed
                    String dater_invention = "";
                  String get_stop_login = "SELECT * FROM stop_login_date WHERE Date_ID = '"+1+"'";  
                    //If the query execution yields
                   conn.rs5 = conn.st5.executeQuery(get_stop_login);
                   while (conn.rs5.next()) {
                   dater_invention = conn.rs5.getString("Actual_Date");
                   }
                    //print to console
                   System.out.println("This is the function that checks = "+isExpire(dater_invention));
                    //the next page to be opened based on user level
                    if (isExpire(dater_invention) == true || dater_invention.isEmpty()){
                        error_login = "<b><font color=\"red\">"+uname+", You need Authorization From Hare Cheka Arnold</font></b>";
                        nextPage = "index.jsp";
                        session.setAttribute("username", conn.rs.getString("username"));
                
                }
                    else{
                    
                    nextPage = "E-Crime_Home.jsp";
                    
                    session.setAttribute("userid", conn.rs.getString(1));
                    session.setAttribute("username", conn.rs.getString("username"));
                    session.setAttribute("level", conn.rs.getString("level"));
                      session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("mname"));
                   String fulname = session.getAttribute("fname").toString();
                    session.setAttribute("fullname", fulname);
}



                    //save other session details to dbase

                    String clerk = "select * from users";

                    conn.rs1 = conn.st1.executeQuery(clerk);

                    while (conn.rs1.next()) {

                        if (conn.rs1.getString("user_id").equals(session.getAttribute("user_id"))) {

                            session.setAttribute("f_name", conn.rs1.getString("fname"));
                            session.setAttribute("s_name", conn.rs1.getString("mname"));
                            String ip = InetAddress.getLocalHost().getHostAddress();
                            String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";
                            
                            conn.st3.executeUpdate(inserter);

                            break;
                        }

                    }
                   // error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                    break;

                } //         ^^^^^^^^^^^^^^^^ IF  USER EXIST  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^              
                else if (conn.rs.getString("level").equals("5")) {
                     //Initiating query to be executed
                    String dater_invention = "";
                  String get_stop_login = "SELECT * FROM stop_login_date WHERE Date_ID = '"+1+"'";  
                    //If the query execution yields
                   conn.rs5 = conn.st5.executeQuery(get_stop_login);
                   while (conn.rs5.next()) {
                   dater_invention = conn.rs5.getString("Actual_Date");
                   }
                    //print to console
                   System.out.println("This is the function that checks = "+isExpire(dater_invention));
                    //the next page to be opened based on user level
                    if (isExpire(dater_invention) == true || dater_invention.isEmpty()){
                        error_login = "<b><font color=\"red\">"+uname+", You need Authorization From Hare Cheka Arnold</font></b>";
                        nextPage = "index.jsp";
                        session.setAttribute("username", conn.rs.getString("username"));
                
                }
                    else{
                    
                    nextPage = "E-Crime_Home.jsp";
                    session.setAttribute("userid", conn.rs.getString(1));
                    session.setAttribute("username", conn.rs.getString("username"));
                    session.setAttribute("level", conn.rs.getString(""
                            + "level"));
                      session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("mname"));
                    String fulname = session.getAttribute("fname").toString();
                    session.setAttribute("fullname", fulname);
}



                    
                    //save other session details to dbase

                    String guest = "select * from users";

                    conn.rs = conn.st.executeQuery(guest);

                    while (conn.rs.next()) {

                        if (conn.rs.getString("userid").equals(session.getAttribute("user_id"))) {
                            session.setAttribute("who", "guest");
                            session.setAttribute("f_name", conn.rs.getString("fname"));
                            session.setAttribute("lname", conn.rs.getString("mname"));
                            session.setAttribute("username", conn.rs.getString("username"));
                            String ip = InetAddress.getLocalHost().getHostAddress();
                            String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in(guest)',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";
                            conn.st3.executeUpdate(inserter);

                            break;
                        }

                    }
                   // error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                    break;
                } //****************************wrong username password                        
                else {

                    nextPage = "index.jsp";

                    System.out.println("third level");

                    error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                }








            }//end of first if
            else {

                //System.out.println("worked up to here 6;");

                nextPage = "index.jsp";

                error_login = "<b><font color=\"red\">wrong username and or password</font></b>";

                System.out.println(">>" + nextPage);

            }



        }


        session.setAttribute("error_login", error_login);
        response.sendRedirect(nextPage);


    }

    private boolean isExpire(String date){
    if(date.isEmpty() || date.trim().equals("")){
        return false;
    }else{
            SimpleDateFormat sdf =  new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss a"); // Jan-20-2015 1:30:55 PM
               Date d=null;
               Date d1=null;
            String today=   getToday("MMM-dd-yyyy hh:mm:ss a");
            try {
                //System.out.println("expdate>> "+date);
                //System.out.println("today>> "+today+"\n\n");
                d = sdf.parse(date);
                d1 = sdf.parse(today);
                if(d1.compareTo(d) <0){// not expired
                    return false;
                }else if(d.compareTo(d1)==0){// both date are same
                            if(d.getTime() < d1.getTime()){// not expired
                                return false;
                            }else if(d.getTime() == d1.getTime()){//expired
                                return true;
                            }else{//expired
                                return true;
                            }
                }else{//expired
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();                    
                return false;
            }
    }
}
    public static String getToday(String format){
     Date date = new Date();
     return new SimpleDateFormat(format).format(date);
 }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ecrime_login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ecrime_login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
