/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_edit_officers extends HttpServlet {

    //Initialising Variables
   String officer_reg_ID;
   
   //Initialising Session
    private HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        //Getting session
        session = request.getSession();
        
        //Creating data object
        ecrime_source conn = new ecrime_source();
        
        /*Receiving parameters of the passed form*/
        officer_reg_ID = request.getParameter("Officer_regID");
        
        //Writing a Query to get the entire details
        String Get_Officer_Details = "SELECT * FROM ecrime_officers_details WHERE Officerme_Cri_ID = '"+officer_reg_ID+"'";
        
        //Executing The Query
        conn.rs = conn.st.executeQuery(Get_Officer_Details);
        
        //While Execution
        while (conn.rs.next()){
            //Putting them into session
            session.setAttribute("Officer_Name", conn.rs.getString("FullName"));
            session.setAttribute("Officer_ID", conn.rs.getString("Officer_ID"));
            session.setAttribute("Officer_Tel", conn.rs.getString("Officer_Tel"));
            session.setAttribute("Officer_Status", conn.rs.getString("Officer_Status"));
        
        }
        session.setAttribute("offic_reg_ID", officer_reg_ID);
        //Redirecting to the editing page
        response.sendRedirect("E-Crime_Officer_Edit.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(ecrime_edit_officers.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(ecrime_edit_officers.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
