/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_finding_verification extends HttpServlet {

    //Initialise Variables
    String Parcel_No;
    int count_lease = 0;
    int count_lease_land = 0;
    
    //Initialising session
    private HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Parcel_No = request.getParameter("Crime_No");
            
            //Creating a new object for the data source
            ecrime_source conn = new ecrime_source();
            
            //Initiating the Query to check if Case is closes
            String Check_Crime_Closure = "SELECT * FROM ecrime_crime_registration WHERE Crime_Number = '"+Parcel_No+"'"
                    + "AND Crime_Status = '"+1+"'";
            String Check_Crime_Closure1 = "SELECT * FROM ecrime_crime_registration WHERE Crime_Number = '"+Parcel_No+"'"
                    + "AND Crime_Status = '"+0+"'";
            
            //Executing the query
            conn.rs = conn.st.executeQuery(Check_Crime_Closure);
            while (conn.rs.next()){
            count_lease = count_lease + 2;
            }
            
            conn.rs1 = conn.st1.executeQuery(Check_Crime_Closure1);
            while (conn.rs1.next()){
            count_lease = count_lease + 1;
            }
            //Accessing the while loop
            if(count_lease == 2){
             session.setAttribute("Yes_Land_Lease", "<font color=\"green\">Crime Number <b>"+Parcel_No+"</b> was solved</font>");
                response.sendRedirect("E-Crime_View_Crime.jsp");
            }
            else if (count_lease == 1){
                session.setAttribute("Finding_Crime", Parcel_No);
                response.sendRedirect("E-Crime_Crime_Findings.jsp");           
            }
            else if (count_lease == 0){
            session.setAttribute("Yes_Land_Lease", "<font color=\"red\">Crime Number <b>"+Parcel_No+"</b> has NEVER been filed</font>");
                response.sendRedirect("E-Crime_View_Crime.jsp");
            }
        count_lease = 0;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_finding_verification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_finding_verification.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
