/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_add_finding extends HttpServlet {

    //INitialisation of variables
    String Crime_No,Witness1,Witness2,Off_Conc,Wet1,Wet2;
    //Initialising session
    private HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        //*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Crime_No = request.getParameter("crime_No");
            Witness1 = request.getParameter("Witness1");
            if (Witness1.isEmpty()){
            Wet1 = "NONE";
            }
            else{
            Wet1 = request.getParameter("Witness1");
            }
            Witness2 = request.getParameter("Witness2");
            if (Witness2.isEmpty()){
            Wet2 = "NONE";
            }
            else{
            Wet2 = request.getParameter("Witness2");
            }
            Off_Conc = request.getParameter("off_conc");
            
            //Creating an object
            ecrime_source conn = new ecrime_source();
            
            
            //Initialising the query
            String finding_add = "UPDATE ecrime_discplinary SET Crime_Officer_Conc = '"+Off_Conc+"', Witness1 = "
                    + "'"+Wet1+"', Witness2 = '"+Wet2+"' WHERE Crime_No = '"+Crime_No+"'";
            
            String chenge_off_stats = "UPDATE ecrime_officers_details SET Officer_Status = '"+0+"' WHERE userid_users = "
                    + "'"+session.getAttribute("userid")+"'";
                        
            conn.st1.executeUpdate(finding_add);
            
            //To print on console
            System.out.println("Officer user ID ="+session.getAttribute("userid"));
            
            conn.st2.executeUpdate(chenge_off_stats);
            
            session.setAttribute("Yes_Land_Lease", "<font color=\"green\">Findings of Crime Number <b>"+Crime_No+"</b> saved successfully</font>");
             
            
             response.sendRedirect("E-Crime_View_Crime.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_add_finding.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_add_finding.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
