/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_crime_details_loader extends HttpServlet {

   //Initialise Variables
    String Crime_No,Lease_Activity;
    int Lease_Count = 0;
    
    //Initialise session
    private HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        //Initialising the session
        session = request.getSession();
        
        //Receiving the parameter
        Crime_No = request.getParameter("crime_id");
        System.out.println("Lease Count Before check ="+Lease_Count);
        
        //Initiating wrangles to null
        Lease_Activity = "";
        
        //Creating a new object for database connection
        ecrime_source conn = new ecrime_source();
        
        //Initialisng the query
        String Get_Crime_Details = "SELECT * FROM ecrime_crime_registration WHERE Crime_Number = '"+Crime_No+"'";
        
        //Executig the Query
        conn.rs=conn.st.executeQuery(Get_Crime_Details);
        
        //Accessing the while loop
        while(conn.rs.next()){
             //Getting the details
        Lease_Activity +=""+getcategory(conn.rs.getString("Crime_Category_ID"))+"|"+conn.rs.getString("Crime_Descrition")+"|"
                    + ""+conn.rs.getString("Accused_Name")+"|"+conn.rs.getString("Complainant_Name")+"|"
                + ""+get_crime_Status(conn.rs.getInt("Crime_Status"))+"|"
                    + ""+get_officer_name(conn.rs.getInt("Crime_Officer"))+"|"+getdiscdate(Crime_No)+"|";
        Lease_Count ++;
        
        }
        System.out.println("Row Count after check ="+Lease_Count);
        
         //Checking the count
        if (Lease_Count == 0){
            
        session.setAttribute("Yes_Land_Lease", "<font color=\"red\">Crime Number "+Crime_No+" has NEVER been Filed</font>");
        }
        Lease_Activity += ""+Lease_Count+"";
        //Closing Connections
        if(conn.rs!=null){
            conn.rs.close();
            }
        if(conn.st!=null){
            conn.st.close();
            }
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>|"+Lease_Activity+"|</h1>");
            out.println("</body>");
            out.println("</html>");
        
       Lease_Count = 0;
       System.out.println("After depositint = "+Lease_Count);
    }
    
    //Function to get Discplinary Date
    public String getdiscdate(String CrimeNo) throws SQLException{
    String DiscpDate = "";
    
    //Creating an Object
    ecrime_source con = new ecrime_source();
    
    //Initialising the QUery
    String GetDis = "SELECT * FROM ecrime_discplinary WHERE Crime_No = '"+CrimeNo+"'";
    
    //Executing the Query
    con.rs3 = con.st3.executeQuery(GetDis);
    
    //Executing the while loop
    while(con.rs3.next()){
    DiscpDate = con.rs3.getString("Discplinary_Date");
    }
        return DiscpDate;
    
            
    }
    
    public String getcategory(String CategoryID) throws SQLException{
        String Category_name = "";
        //Creating an object
        ecrime_source connn = new ecrime_source();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM ecrime_category WHERE Category_ID = '"+CategoryID+"'";
        
        /*Executing the Query*/
        connn.rs1 = connn.st1.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs1.next()){
        Category_name = connn.rs1.getString("Category_Name");
        }
       return Category_name;
         
    }
     public String get_crime_Status(int status_ID){
        //Iniitialisng variables
        String Status_Name = "";
        if (status_ID == 0){
        Status_Name = "UNSOLVED";
        }
        else {
        Status_Name = "SOLVED";
        }
       return Status_Name;
    }

     public String get_officer_name (int officer_ID) throws SQLException{
     String officer_name = "";
        //Creating an object
        ecrime_source connn = new ecrime_source();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM ecrime_officers_details WHERE Officerme_Cri_ID = '"+officer_ID+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("FullName");
        }
       return officer_name;
     }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_crime_details_loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ecrime_crime_details_loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
