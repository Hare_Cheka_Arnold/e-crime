/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_discplinary_date extends HttpServlet {

    //Initialising Variables
   String Crime_Reg_ID,Crime_Number;
   
   //Initialising Session
    HttpSession session;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        //Getting session
        session = request.getSession();
        
        //Creating data object
        ecrime_source conn = new ecrime_source();
        
        /*Receiving parameters of the passed form*/
        Crime_Reg_ID = request.getParameter("Crime_Reg_ID");
        Crime_Number = request.getParameter("Crime_Number");
        
        //Writing a Query to get the Crime entire details and involved personel
        String Getting_Crime_Details = "SELECT * FROM ecrime_crime_registration WHERE Crime_Number = '"+Crime_Number+"'"
                + " AND Crime_Registration_ID = '"+Crime_Reg_ID+"'";
        
        
        
        /*Executing the Query*/
        conn.rs = conn.st.executeQuery(Getting_Crime_Details);
        
        //Accessing a while loop
        while(conn.rs.next()){
            //Putting details into session
            session.setAttribute("Crime_Reg", Crime_Reg_ID);
            session.setAttribute("Crime_Number", Crime_Number);
            String Crime_Category_ID = conn.rs.getString("Crime_Category_ID");          
            
            session.setAttribute("Crime_Category", getcategory(Crime_Category_ID));
            session.setAttribute("Crime_Description", conn.rs.getString("Crime_Descrition"));
            session.setAttribute("Crime_Accused", conn.rs.getString("Accused_Name"));
            session.setAttribute("Crime_Complainant", conn.rs.getString("Complainant_Name"));
            session.setAttribute("Report_Date", conn.rs.getString("Report_Date"));
            session.setAttribute("Crime_Status", get_crime_Status(conn.rs.getInt("Crime_Status")));
            session.setAttribute("Crime_Officer", get_officer_name(conn.rs.getInt("Crime_Officer")));
            
            
        
        }
        //Redirecting to the editing page
        response.sendRedirect("E-Crime_Discplinary_Date.jsp");
        
    }
    
    public String getcategory(String CategoryID) throws SQLException{
        String Category_name = "";
        //Creating an object
        ecrime_source connn = new ecrime_source();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM ecrime_category WHERE Category_ID = '"+CategoryID+"'";
        
        /*Executing the Query*/
        connn.rs1 = connn.st1.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs1.next()){
        Category_name = connn.rs1.getString("Category_Name");
        }
       return Category_name;
         
    }
     public String get_crime_Status(int status_ID){
        //Iniitialisng variables
        String Status_Name = "";
        if (status_ID == 0){
        Status_Name = "UNSOLVED";
        }
        else {
        Status_Name = "SOLVED";
        }
       return Status_Name;
    }

     public String get_officer_name (int officer_ID) throws SQLException{
     String officer_name = "";
        //Creating an object
        ecrime_source connn = new ecrime_source();
        
        //Initialising the query
        String Get_Category = "SELECT * FROM ecrime_officers_details WHERE Officerme_Cri_ID = '"+officer_ID+"'";
        
        /*Executing the Query*/
        connn.rs2 = connn.st2.executeQuery(Get_Category);
        
        //Accessing the while loop
        while(connn.rs2.next()){
        officer_name = connn.rs2.getString("FullName");
        }
       return officer_name;
     }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(ecrime_discplinary_date.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
           processRequest(request, response);
       } catch (SQLException ex) {
           Logger.getLogger(ecrime_discplinary_date.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
