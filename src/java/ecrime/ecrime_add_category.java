/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecrime;

import ecrime_datasource.ecrime_source;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ecrime_add_category extends HttpServlet {

    /*Initialising variable*/
    HttpSession session;
    String Crime_Category, Crime_Category_Description;
    boolean statuz = false;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");

//*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Crime_Category = request.getParameter("category_name");
            Crime_Category_Description = request.getParameter("category_des");
            
            
            //Creating a new object for the data source
            ecrime_source conn = new ecrime_source();
            
            //Initialising the query to insert new counties.                      

            String save_category = "INSERT INTO ecrime_category (Category_Name,Category_Description) "
                    + "VALUES ('" + Crime_Category + "','" + Crime_Category_Description + "')";
            
            //When description is empty or null
            String save_category1 = "INSERT INTO ecrime_category (Category_Name) "
                    + "VALUES ('" + Crime_Category + "')";
            
            //Initialising the query to check if the county exists

            String category_checker = "SELECT * FROM ecrime_category WHERE Category_Name='" + Crime_Category + "'";


            //Statement to execute the query to check
            conn.rs = conn.st.executeQuery(category_checker);

            //check if ward is already registered 
            if (!conn.rs.next()) {
                
                if (!(Crime_Category_Description == null || Crime_Category_Description.equalsIgnoreCase("null") || 
                        Crime_Category_Description.isEmpty())){
                //if category does not exist and category description is not null execute the query to insert new couty
                conn.st1.executeUpdate(save_category);
                }
                else{
                    //if category does not exist and category description is null execute the query to insert new couty
                    conn.st1.executeUpdate(save_category1);
                }
                

                session.setAttribute("County_added", "<font color=\"green\">Crime Category "+Crime_Category+" added succesfully</font>");
            } else {
                session.setAttribute("County_added", "<b><font color=\"red\">Sorry, Crime Category "+Crime_Category+" is already registered.</font></b>");


            }
            
            response.sendRedirect("E-Crime_Add_Category.jsp");

        } catch (SQLException ex) {
            Logger.getLogger(ecrime_add_category.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
