<%-- 
    Document   : E-Crime_Officer_Edit
    Created on : 21-Aug-2015, 01:32:52
    Author     : user
--%>

<%@page import="ecrime_datasource.ecrime_source"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>E-Crime Edit Officer.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader




function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        
    </head>
    <body onload="load_counties();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>


              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 20px; text-align: center;">Editing E-Crime Officer.</div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("editing_ecrime_officer") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("editing_ecrime_officer")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("editing_ecrime_officer");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
 <form action="ecrime_adit_officer" method="post">
                      <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:190px ;">
                      
    <tr>
                             <input id="userid"  type=hidden  readonly name="userid" value="<%=session.getAttribute("offic_reg_ID")%>" student_name class="textbox"/>                            
                            <tr>
                                <td class="align_button_left"><label for="first_name">Full Name<font color="red">*</font></label></td>
                                <td><input id="full_name" type=text required name="full_name" student_name value="<%=session.getAttribute("Officer_Name")%>" class="textbox"/></td></tr><tr>

                               
                            </tr>
                            
                            
                            <tr>
                                <td class="align_button_left"><label for="first_name">Phone Number<font color="red">*</font></label></td>
                                <td><input id="phone_number" type=text pattern="[0-9]{10,10}" maxlength="10" placeholder="07xxxxxxxx" required name="phone_number" value="<%=session.getAttribute("Officer_Tel")%>" student_name class="textbox"/></td></tr><tr>

                               
                            </tr>
                           
                            <tr>
                                <td class="align_button_left"><label for="first_name">ID Number<font color="red">*</font></label></td>
                                <td><input id="officerID" readonly="true" type=text required name="officerID" student_name value="<%=session.getAttribute("Officer_ID")%>" class="textbox"/></td></tr><tr>

                               
                            </tr>
                            <tr>
                                <td class="align_button_left">
                                    <label for="first_name">Officer's Status<font color="red">*</font></label
                                 </td>
                                <td>
                            <select name="officer_Status" id="officer_Status"  class="textbox10 " onchange="user_disablation();" required="true">
                                <option value="">Choose Status</option>                                                       
                                        <option value="0">Free</option> 
                                        <option value="1">Assigned</option>
                                        <option value="2">Suspended</option>
                            </select>
                                </td>
                            </tr>
                                                        
                            
            
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           <tr> 
                               <td class="align_button_left"><input  size="12px"  type="reset" value="clear" /></td> <td class="align_button_right">
                               <input type="submit" class="submit" value="Update Details" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy E-CRIME SYSTEM <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>

