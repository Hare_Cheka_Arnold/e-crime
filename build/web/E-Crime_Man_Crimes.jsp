<%-- 
    Document   : E-Crime_Man_Crimes
    Created on : 21-Aug-2015, 02:34:47
    Author     : user
--%>

<%@page import="ecrime_datasource.ecrime_source"%>
<%@page import="org.json.JSONObject"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>E-Crime Manage Crimes.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader




function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        
    </head>
    <body onload="load_counties();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 20px; text-align: center;">Manage Crimes. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Man_Crime_edited") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Man_Crime_edited")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Man_Crime_edited");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <br/>
                    <form action="ecrime_discplinary_date" method="post">
                        <br/>
                        
                        <table class="table_style2" cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:20px ;">
                            <thead>
                                <tr>
                                    <th>
                                        Crime Number
                                    </th>
                                    <th>
                                        Crime Description
                                    </th>
                                    <th>
                                        Accused Name
                                    </th>
                                    <th>
                                        Complainant Name
                                    </th>
                                    <th>
                                        Report Date
                                    </th>
                                    <th>
                                        Officer Assigned
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                 ecrime_source conn = new ecrime_source();
                                String Display_Land_Details = "SELECT * FROM ecrime_crime_registration WHERE Crime_Status"
                                        + "= 0";
                                conn.rs = conn.st.executeQuery(Display_Land_Details);
                                while (conn.rs.next()) {%>
                                
                                <tr>
                                   
                                    <td>
                                         
                                  <%= conn.rs.getString("Crime_Number") %>
                                  </td>
                                  
                                  <td>
                                  <%= conn.rs.getString("Crime_Descrition") %>
                                  </td>
                                  <td>
                                  <%= conn.rs.getString("Accused_Name") %>
                                  </td>
                                  <td>
                                     <%= conn.rs.getString("Complainant_Name") %>
                                  </td>
                                  <td>
                                      <%= conn.rs.getString("Report_Date") %>
                                  </td>
                                  <%
                                  String Officer_Name = "";
                                  String ftch_data= "SELECT * FROM ecrime_officers_details WHERE "
                                          + "Officerme_Cri_ID = '"+conn.rs.getString("Crime_Officer")+"'";
                                  //Executing Query
                                  conn.rs1 = conn.st1.executeQuery(ftch_data);
                                  while(conn.rs1.next()){
                                  Officer_Name = conn.rs1.getString("FullName");
                                  }
                                  //conn.rs1.close();
                                  %>
                                  <td>
                                  <%= Officer_Name %>
                                  </td>
                                  <td>
                                      
                                  <form action="ecrime_discplinary_date" method="post">
                                          <input type="hidden" name="Crime_Reg_ID" id="Crime_Reg_ID" value="<%= conn.rs.getString("Crime_Registration_ID") %>">
                                          <input type="hidden" name="Crime_Number" id="Crime_Number" value="<%= conn.rs.getString("Crime_Number") %>">
                                          <input type="submit" class="submit" value="Assign Discp' Date"/>
                                  </form>
                                  </td>
                                </tr>
                                    
                               <% }%>
       
    
    
                            </tbody>
                        </table>
                    </form>
                </div>
      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>     
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;">&copy E-CRIME SYSTEM  <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
