<%-- 
    Document   : E-Crime_User_Registration
    Created on : 11-Aug-2015, 00:57:24
    Author     : user
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>E-CRIME REGISTRATION.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
         
$(function() {
$( "#datepicker" ).datepicker();
});

//********************************************FUNCTION TO LOAD COUNTIES************************************
function load_faculties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
user_disablation();
document.getElementById("Faculty").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","ecrime_faculty_loader",true);
xmlhttp.send();
}
//county loader
//**********************************FILTERING DEPARTMENTS******************************************************************
function filter_departments(Faculty){
var Faculty_type=Faculty.value; 
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("Department").innerHTML= xmlhttp.responseText;;

}
}
xmlhttp.open("POST","ecrime_department_loader?faculty_type="+Faculty_type,true);
xmlhttp.send();
}


function user_disablation() {    
  var v= document.getElementById("user_type").value;
  var course_details = document.getElementById("course_details");
  var staff_details = document.getElementById("staff_details");
  var course_sectit = document.getElementById("course_sectit");
  var Title_No = document.getElementById("Title_No");
  var userid_label = document.getElementById("userid_label");
  var userid = document.getElementById("userid");
  var Reg_No_Label = document.getElementById("Reg_No_Label");
   var Reg_No = document.getElementById("Reg_No");
  var Emp_No_Label = document.getElementById("Emp_No_Label");
  var Emp_No = document.getElementById("Emp_No");
  var Faculty_Label = document.getElementById("Faculty_Label");
   var Faculty = document.getElementById("Faculty");
   var Department_Label = document.getElementById("Department_Label");
   var Department = document.getElementById("Department");
   var year_label = document.getElementById("year_label");
   var Course_Year = document.getElementById("Course_Year");
   
   //First Section
   var student_personal = document.getElementById("student_personal");
   var lecturer_personal = document.getElementById("lecturer_personal");
   var Full_Name_Label = document.getElementById("Full_Name_Label");
   var Full_Name = document.getElementById("Full_Name");
   var ID_NO_Label = document.getElementById("ID_NO_Label");
   var ID_NO = document.getElementById("ID_NO");
   var Tel_No_Label = document.getElementById("Tel_No_Label");
   var Tel_No = document.getElementById("Tel_No");
   var Email_Label = document.getElementById("Email_Label");
   var Email = document.getElementById("Email");
   var Reg_Date_Label = document.getElementById("Reg_Date_Label");
   var Reg_Date = document.getElementById("Reg_Date");
   
   
   
//  alert(" geek     :  "+v)
if (v == ""){
  course_details.hidden = true; 
  staff_details.hidden = true; 
  course_sectit.hidden = true;
  Title_No.hidden = true;
  userid_label.hidden = true; 
  userid.hidden = true; 
  Reg_No_Label.hidden = true;
  Reg_No.hidden = true;
  Emp_No_Label.hidden = true; 
  Emp_No.hidden = true;
  Faculty_Label.hidden = true;
  Faculty.hidden = true;
  Department_Label.hidden = true;
  Department.hidden = true; 
  Course_Year.hidden = true;
  year_label.hidden = true;
   student_personal.hidden = true;
  lecturer_personal.hidden = true;
  Full_Name_Label.hidden = true; 
  Full_Name.hidden = true;
  ID_NO_Label.hidden = true;
  ID_NO.hidden = true;
  Tel_No_Label.hidden = true;
  Tel_No.hidden = true; 
  Email_Label.hidden = true;
  Email.hidden = true;
  Reg_Date_Label.hidden = true;
  Reg_Date.hidden = true; 
  
  
}
if (v == 1){
   course_details.hidden = false; 
  staff_details.hidden = true; 
  course_sectit.hidden = false;
  Title_No.hidden = false;
  Title_No.required = true;
  userid_label.hidden = true; 
  userid.hidden = true;
  userid.required = true;
  Reg_No_Label.hidden = false;
  Reg_No.hidden = false;
  Reg_No.required = true;
  Emp_No_Label.hidden = true; 
  Emp_No.hidden = true;
  Emp_No.required = false;
  Faculty_Label.hidden = false;
  Faculty.hidden = false;
  Faculty.required = true;
  Department_Label.hidden = false;
  Department.hidden = false;
  Department.required = true;
  Course_Year.hidden = false;
  Course_Year.required = true;
  year_label.hidden = false;
  student_personal.hidden = false;
  lecturer_personal.hidden = true;
  Full_Name_Label.hidden = false; 
  Full_Name.hidden = false;
  ID_NO_Label.hidden = false;
  ID_NO.hidden = false;
  Tel_No_Label.hidden = false;
  Tel_No.hidden = false;
  Email_Label.hidden = false;
  Email.hidden = false;
  Reg_Date_Label.hidden = false;
  Reg_Date.hidden = false;
 
}
if(v == 2) {
  course_details.hidden = true; 
  staff_details.hidden = false; 
  course_sectit.hidden = true;
  Title_No.hidden = true;
  Title_No.required = false;
  userid_label.hidden = false; 
  userid.hidden = false;
  userid.required = true;
  Reg_No_Label.hidden = true;
  Reg_No.hidden = true;
  Reg_No.required = false;
  Emp_No_Label.hidden = false; 
  Emp_No.hidden = false;
  Emp_No.required = true;
  Faculty_Label.hidden = false;
  Faculty.hidden = false;
  Faculty.required = true;
  Department_Label.hidden = false;
  Department.hidden = false;
  Department.required = true;
  Course_Year.hidden = true;
  Course_Year.required = false;
  year_label.hidden = true;
  student_personal.hidden = true;
  lecturer_personal.hidden = false;
  Full_Name_Label.hidden = false; 
  Full_Name.hidden = false;
  ID_NO_Label.hidden = false;
  ID_NO.hidden = false;
  Tel_No_Label.hidden = false;
  Tel_No.hidden = false; 
  Email_Label.hidden = false;
  Email.hidden = false;
  Reg_Date_Label.hidden = false;
  Reg_Date.hidden = false;
  
  //
  }
if (v==5 || v==6 || v==7) {
 year_label.hidden = true; 
  year.hidden = true; 
  year.required = false;
  year_gen_gen.hidden = true;
  year_clear.hidden = true; 
  date_range_table.hidden = false; 
  Start_date_label.hidden = false;
  start_date.hidden = false;
  start_date.required = true; 
  end_date_label.hidden = false;
  end_date.hidden = false;
  end_date.required = true;
  date_clear.hidden = false;
  date_gen.hidden = false;
  parcel_no_table.hidden = true;
  parcel_no_label.hidden = true;
  TiPaPl_No.hidden = true;
  TiPaPl_No.required = false;
  }
  }

      </script> 
        <SCRIPT language=Javascript>
		<!--
function isCharacterKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return true;
return false;
}

function isNumberKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}

//-->
</SCRIPT>
    </head>
    <body onload="load_faculties();">
       
     <div id="container" style="height:auto;" >
<%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 24px; text-align: center;">User registration. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("User_Registration") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("User_Registration")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("User_Registration");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="ecrime_user_registration" method="post">
                        <div class="Main">
                            <table cellpadding="2px" cellspacing="4px" border="0px" style="margin-left:150px ;">
                                <tr>
                                    <td>
                                      <label for="first_name" class="lbl"><em class="xoxo">User Type</em><font color="red">*</font></label>  
                                    </td>
                                    <td>
                                      <select name="user_type" id="user_type"  class="textbox2 " onchange="user_disablation();" required="true">
                                        <option value="">Choose User Type</option>                                                       
                                        <option value="1">Student</option> 
                                        <option value="2">Lecturer</option>
                                        <!--<option value="3">Other</option>-->
                                    </select>  
                                    </td>
                                </tr>
                            </table>
                            <div class="sub_main_one">
                                 <%
DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                                <label for="first_name" id="student_personal" ><em class="soso">STUDENT'S DETAILS</em><font color="red"></font></label>
                                <label for="first_name" id="lecturer_personal" ><em class="soso">LECTURER'S DETAILS</em><font color="red"></font></label>
                                <table cellpadding="2px" cellspacing="4px" border="0px" style="margin-left:10px ;">
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Full_Name_Label" ><em class="xoxo">Full Names:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Full_Name" onkeypress="return isCharacterKey(event)" type=text required ="true" name="Full_Name" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="ID_NO_Label" ><em class="xoxo">ID NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="ID_NO"  type=text required ="true" onkeypress="return isNumberKey(event)" name="ID_NO" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Tel_No_Label" ><em class="xoxo">Tel NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                            <input id="Tel_No"  type=text required ="true" pattern="[0-9]{10,10}" maxlength="10" name="Tel_No" placeholder="Input 07xxxxxxxx" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Email_Label" ><em class="xoxo">Email:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Email"  type=email required ="true" name="Email" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                   <!-- <tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Website:</em><font color="red"></font></label>  
                                        </td>
                                        <td>
                                          <input id="Website"  type=text name="Website" student_name class="textbox"/>  
                                        </td>
                                    </tr>-->
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Reg_Date_Label" ><em class="xoxo">Registration Date:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                            <input id="Reg_Date" readonly="true" type=text name="Reg_Date" value="<%=dateFormat.format(cal.getTime())%>" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                </table>
                                </div>
                            <div class="sub_main_two">
                                <label for="first_name" id="course_details" ><em class="soso">COURSE DETAILS</em><font color="red"></font></label>
                               <label for="first_name" id="staff_details" ><em class="soso">STAFF DETAILS</em><font color="red"></font></label>
                                <table cellpadding="2px" cellspacing="4px" border="0px" style="margin-left:10px ;">
                                    <tr>
                                        <td>
                                           <label for="first_name" id="course_sectit" ><em class="xoxo">Course Title:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Title_No"  type=text  name="Title_No" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="userid_label" ><em class="xoxo">User ID:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="userid"  type=text  readonly name="userid" value="<%=generateRandomNumber(3941,1870888)%>" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Reg_No_Label" ><em class="xoxo">Registration NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Reg_No"  type=text  name="Reg_No" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Emp_No_Label" ><em class="xoxo">Employee NO:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Emp_No"  type=text  name="Emp_No" student_name class="textbox"/>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Faculty_Label" ><em class="xoxo">Faculty</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <select name="Faculty" id="Faculty"  class="textbox10 " onchange="filter_departments(this);" >
                                         
                                        
                                    </select>  
                                        </td>
                                    </tr>
                                    
                                     <!--<tr>
                                        <td>
                                           <label for="first_name" class="lbl"><em class="xoxo">Location:</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <input id="Location"  type=text required="true" name="Location" student_name class="textbox"/>  
                                        </td>
                                    </tr>-->
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="Department_Label" ><em class="xoxo">Department</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                          <select name="Department" id="Department"  class="textbox10 " >
                                             <option value="">Choose Department</option>
                                        
                                    </select>  
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                           <label for="first_name" id="year_label" ><em class="xoxo"> Year of Course</em><font color="red">*</font></label>  
                                        </td>
                                        <td>
                                         <textarea name="Course_Year" id="Course_Year"  value=""   rows="4" column="6" ></textarea>  
                                        </td>
                                    </tr>
                                    
                                </table>
                            </div>
                            <table cellpadding="0px" cellspacing="4px" border="0px" style="margin-left:90px ;">
                            <tr> 
                               <td class="align_button_left">
                                   <input  size="12px"  type="reset" value="clear" />
                               </td> 
                               
                               <td>
                                   
                               </td>
                               
                               <td>
                                   
                               </td>
                            
                               <td class="align_button_right">
                               <input type="submit" class="submit1" value="Register" onmouseover="getAge();"/>
                               </td>
                               
                            </tr>
                            </table>
                            <div style="clear: both">
                                
                            </div>
                        </div>
                                     
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;">&copy E-CRIME SYSTEM  <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>

