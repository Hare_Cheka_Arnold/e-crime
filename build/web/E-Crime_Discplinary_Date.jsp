<%-- 
    Document   : E-Crime_Discplinary_Date
    Created on : 22-Aug-2015, 04:31:54
    Author     : user
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>E-Crime Discplinary Assignment</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

       function load_categories(){    
var crime_status = document.getElementById("crime_status").value;
var crime_button = document.getElementById("Disc_date");
alert (crime_status);
if (crime_status == "SOLVED"){
    crime_button.disabled = true;
}else{
    crime_button.disabled = false;
}

}

//Detectives Loader
function load_detectives(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("Assigned_officer").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","ecrime_officers_loader",true);
xmlhttp.send();
}



function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        <SCRIPT language=Javascript>
		<!--
function isCharacterKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return true;
return false;
}

function isNumberKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}

//-->
</SCRIPT>
       <script src="jss/jquery-1.7.2.js"></script>
	<script src="jss/jquery-ui-1.10.3.custom.js"></script>
 <link href="jss/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet">
       <script src="jss/datepicker.js"></script>
       <script src="admin/ui/jquery.ui.datepicker.js"></script>
        <script>	
                $(function() {
        $( ".datepicker" ).datepicker({
                                dateFormat: "dd/mm/yy",
                                changeMonth: true,
                                changeYear: true
                               
                        });
                    
                });
            </script>  
    </head>
    <body onload="check_status();">
       
     <div id="container" style="height:auto;" >
<%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%>
                <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 20px; text-align: center;"><b>E-CRIME DISCPLINARY DATE ASSIGNMENT. </b></div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Crime_Displinary") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Crime_Displinary")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Crime_Displinary");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red"></font>Discplinary Assignment With E-CRIME SYSTEM</p>
                     <form action="ecrime_discplinary_assign" method="post">
                          <%
DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                    <br/>
                    <table cellpadding="2px" cellspacing="5px" border="0px" style="margin-left:205px ;">
                        <tr>                            
                                <td class="align_button_left">
                                    <label for="first_name">Crime NO<font color="red">*</font></label>
                                </td>
                                <td>
                                    <input id="CrimeNo" type=text required="true" readonly="true" name="CrimeNo" value="<%=session.getAttribute("Crime_Number")%>" student_name class="textbox"/>
                                </td>
                               
                            
                        </tr>
                    </table>
                        <table cellpadding="2px" cellspacing="5px" border="0px" style="margin-left:40px ;">
                      
    <tr>
                                                       
                            <tr>
                                <td class="align_button_left"><label for="first_name">Crime Category<font color="red">*</font></label></td>
                                <td>
                                     <input id="CrimeCategory" type=text required="true" readonly="true" name="CrimeCategory" value="<%=session.getAttribute("Crime_Category")%>" student_name class="textbox"/>
                              
                                </td>
                                 <td class="align_button_left"><label for="first_name">Crime Description<font color="red">*</font></label></td>
                                 <td><input type="textarea" readonly="true" name="crimedesc" id="crimedesc" value="<%=session.getAttribute("Crime_Description")%>" class="textbox"  rows="4" column="30" required></textarea></td>

                            </tr>
                            
                                                  
                            <tr>
                                <td class="align_button_left"><label for="first_name">Accused Names<font color="red"></font></label></td>
                                <td><input id="AccusedName" type=text readonly="true" name="AccusedName" value="<%=session.getAttribute("Crime_Accused")%>" student_name class="textbox"/></td>
                                 <td class="align_button_left"><label for="first_name">Complainant Names<font color="red"></font></label></td>
                                <td><input id="complainantname" type=text readonly="true" name="complainantname" value="<%=session.getAttribute("Crime_Complainant")%>" student_name class="textbox"/></td>
                           
                            </tr>
                           
                           
                            <tr>
                                <td class="align_button_left"><label for="first_name">Report Date<font color="red">*</font></label></td>
                                <td><input id="reportdate" type=text required name="reportdate" readonly="true" student_name value="<%=session.getAttribute("Report_Date")%>" class="textbox"/></td>
                                <td class="align_button_left"><label for="first_name">Assigned Officer<font color="red">*</font></label></td>
                                <td>
                                  <input id="assigned_off" type=text required name="assigned_off" readonly="true" student_name value="<%=session.getAttribute("Crime_Officer")%>" class="textbox"/>  
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="align_button_left"><label for="first_name">Crime Status<font color="red">*</font></label></td>
                                <td><input id="crime_status" type=text required name="crime_status" readonly="true" student_name value="<%=session.getAttribute("Crime_Status")%>" class="textbox"/></td>
                                <td class="align_button_left"><label for="first_name">Discplinary Date<font color="red">*</font></label></td>
                                <td>
                                 <input id="discp_date" type=text class="datepicker textbox" onclick="return checkdate();" value="" required name="discp_date" student_name />
                                </td>
                            </tr>
                            
                        
                           <tr> 
                               <td></td>
                               <td class="align_button_left"><input  size="12px"  type="reset" value="clear" /></td>
                               
                               <td></td>
                               <td class="align_button_left"><input type="submit" id="Disc_date" class="submit" value="Assign Disc' Date" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy E-CRIME SYSTEM <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
