<%-- 
    Document   : adminmenu
    Created on : Feb 2, 2014, 7:19:35 PM
    Author     : SIXTYFOURBIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	
	<title>css3menu.com</title>
		<!-- Start css3menu.com HEAD section -->
	<link rel="stylesheet" href="adminmenu_files/css3menu1/style.css" type="text/css" /><style type="text/css">._css3m{display:none}</style>
	<!-- End css3menu.com HEAD section -->

	
</head>
<body style="background-color:#EBEBEB">
<!-- Start css3menu.com BODY section -->
<ul id="css3menu1" class="topmenu">
	<li class="topmenu"><a href="logout.jsp" style="width:96px;height:21px;line-height:21px;"><span>Attendance</span></a>
	<ul>
		<li class="subfirst"><a href="#">Mark attendance</a></li>
		<li class="sublast"><a href="#">Edit attendance</a></li>
	</ul></li>
	<li class="topmenu"><a href="newClerk.jsp" title="system users" style="width:98px;height:21px;line-height:21px;"><span>Users</span></a>
	<ul>
		<li class="subfirst"><a href="#">add user</a></li>
		<li><a href="#">edit user</a></li>
		<li class="sublast"><a href="#">remove   user</a></li>
	</ul></li>
	<li class="topmenu"><a href="#" style="width:97px;height:21px;line-height:21px;"><span>Chws</span></a>
	<ul>
		<li class="subfirst"><a href="filtercus.jsp">add new  chw</a></li>
		<li><a href="editchw.jsp">edit a chw</a></li>
		<li class="sublast"><a href="#">Delete chw</a></li>
	</ul></li>
	<li class="topmenu"><a href="payments.jsp" style="width:96px;height:21px;line-height:21px;"><span>Payments</span></a>
	<ul>
		<li class="sublast"><a href="#">payment files</a></li>
	</ul></li>
	<li class="topmenu"><a href="#" style="width:97px;height:21px;line-height:21px;"><span>Reports</span></a>
	<ul>
		<li class="subfirst"><a href="#">Attendance Ratio</a></li>
		<li><a href="#"><span>meeting venues</span></a>
		<ul>
			<li class="subfirst"><a href="#">view map</a></li>
			<li class="sublast"><a href="#">view excel file</a></li>
		</ul></li>
		<li class="sublast"><a href="#">attendance history</a></li>
	</ul></li>
	 <li class="topmenu"><a href="PPT/USERGUIDE.pptx" style="width:152px;height:21px;line-height:21px;">Help</a></li>
       
	<li class="topmenu"><a href="logout.jsp" style="width:97px;height:21px;line-height:21px;">Log out</a></li>
</ul><p class="_css3m"><a href="http://css3menu.com/">CSS Menu</a> by Css3Menu.com</p>
<!-- End css3menu.com BODY section -->

</body>
</html>
