<%-- 
    Document   : E-Crime_View_Crime
    Created on : 22-Aug-2015, 23:14:21
    Author     : user
--%>

<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%
String thewrangles = "";
String thewranglescount = "";
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>Crime Operations.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});


function load_wrangles_table(){
 document.getElementById("Land__In_Wrangles").hidden = true;
document.getElementById("Land_wrangles_title").hidden = true;
var level = document.getElementById("userid").value;

if (level == 1 || level == 10){
    document.getElementById("find").hidden = false;
}else{
   document.getElementById("find").hidden = true; 
}
}


function land_lease_woes(){

var dist=document.getElementById("Crime_No").value;    
//alert(dist);
// window.open("districtchooser?county="+dist.value);     
var xmlhttp;  

var elmtTable = document.getElementById("Land__In_Wrangles");
var tableRows = elmtTable.getElementsByTagName("tr");
var rowCount = tableRows.length;

for (var x=rowCount-1; x>0; x--) {
   elmtTable.removeChild(tableRows[x]);
}
//When the input box is empty
if (dist=="")
{
document.getElementById("Land__In_Wrangles").hidden = true;
document.getElementById("Land_wrangles_title").hidden = true;  
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
 
var n = xmlhttp.responseText;
//To split them
mit = n.split("|");
document.getElementById("Land__In_Wrangles").hidden = false;
document.getElementById("Land_wrangles_title").hidden = false;
//saddRow(mit[3],mit[4])
//Assigning
if (mit[8]>0){
tabBody=document.getElementsByTagName("tbody").item(1);
row=document.createElement("tr");
cell1 = document.createElement("td");
cell2 = document.createElement("td");
cell3 = document.createElement("td");
cell4 = document.createElement("td");
cell5 = document.createElement("td");
cell6 = document.createElement("td");
cell7 = document.createElement("td");
textnode1=document.createTextNode(mit[1]);
textnode2=document.createTextNode(mit[2]);
textnode3=document.createTextNode(mit[3]);
textnode4=document.createTextNode(mit[4]);
textnode5=document.createTextNode(mit[5]);
textnode6=document.createTextNode(mit[6]);
textnode7=document.createTextNode(mit[7]);
cell1.appendChild(textnode1);
cell2.appendChild(textnode2);
cell3.appendChild(textnode3);
cell4.appendChild(textnode4);
cell5.appendChild(textnode5);
cell6.appendChild(textnode6);
cell7.appendChild(textnode7);
row.appendChild(cell1);
row.appendChild(cell2);
row.appendChild(cell3);
row.appendChild(cell4);
row.appendChild(cell5);
row.appendChild(cell6);
row.appendChild(cell7);
tabBody.appendChild(row);

}
else{
    
    window.location="E-Crime_View_Crime.jsp";
//    document.getElementById("Land__In_Wrangles").hidden = true;
//    document.getElementById("Land_wrangles_title").hidden = true;
}
}
}
xmlhttp.open("POST","ecrime_crime_details_loader?crime_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
  

//********************************************Function To Populate The Table Rows********************************************
function addRow(content,morecontent)
{
         if (!document.getElementsByTagName) return;
         tabBody=document.getElementsByTagName("tbody").item(0);
         row=document.createElement("tr");
         cell1 = document.createElement("td");
         cell2 = document.createElement("td");
         textnode1=document.createTextNode(content);
         textnode2=document.createTextNode(morecontent);
         cell1.appendChild(textnode1);
         cell2.appendChild(textnode2);
         row.appendChild(cell1);
         row.appendChild(cell2);
         tabBody.appendChild(row);


}
//********************************************End of Function*****************************************************************
        </script> 
        
    </head>
    <body onload="load_wrangles_table();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 20px; text-align: center;">Crime Operations. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Yes_Land_Lease") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Yes_Land_Lease")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Yes_Land_Lease");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="ecrime_finding_verification" method="post">
                        <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:150px ;">
                      
    <tr>
        <input id="userid"  type=hidden  readonly name="userid" value="<%=session.getAttribute("level")%>" student_name class="textbox"/>  
                                                       
                            <tr>
                                <td class="align_button_left"><label for="first_name">Crime No<font color="red">*</font></label></td>
                                <td><input id="Crime_No" type=text required name="Crime_No"  student_name class="textbox"/></td>
                                <td></td>
                            </tr>
                            
                            <tr> 
                               <td class="align_button_left"><input  size="12px"  type="reset" value="clear" /></td>
                               <td class="align_button_left"><input type="button" style="margin-left:20px ;" class="submit" value="Enquire" onclick="return land_lease_woes()"/></td>
                               <td class="align_button_left"><input type="submit" id="find" class="submit" value="Findings" /></td>
                            </tr>
                                      
                            
            
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           
                        </table><br>
                        <label for="first_name" id="Land_wrangles_title"><b>CRIME DETAILS</b><font color="red">*</font></label>
<table class="table_style2" id="Land__In_Wrangles" style="margin-left:25px ;">
    <thead>
        <tr>
            <th>
                CRIME CATEGORY
            </th>
            
            <th>
                DESCRIPTION
            </th>
            <th>
                ACCUSED
            </th>
            <th>
                COMPLAINANT
            </th>
            <th>
               STATUS
            </th>
            <th>
                INVESTIGATING OFFICER
            </th>
            <th>
                DISCPLINARY DATE
            </th>
        </tr>
    </thead>
    <tbody id="Land__In_Wrangles_body">
        
    </tbody>
</table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;">&copy LAND MANAGEMENT SYSTEM  <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>
