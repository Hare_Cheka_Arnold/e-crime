<%-- 
    Document   : index
    Created on : May 10, 2015, 11:35:54 AM
    Author     : Hare Cheka Arnold
--%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>E-CRIME SYSTEM</title>
        <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel='stylesheet' type='text/css' href='css/main.css' />
        <script src="prefix-free.js"></script>
   
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
    </head>
    <body>
        <div id="container" style=" height: 550px">

            <div id="content" style=" height:530px">
                
                <div id="midcontent" style=" height: 530px">
                      
                    <img src="images/ecrime_title_foto.png" width="700px" alt="W.M.S System" align="centre"
                                               style="position: relative; top:50px; left: 150px; height: 130px;"/>
                                              
                       <div style="position: relative; top: 70px; left: 100px;">                                       
                           <form action="ecrime_login" method="post" style=" margin-left: 150px;width: 500px;">
                       <p align="center">Login Here.</p>
                        <table style="margin-left: 170px; width: 500px;" >
                           
                            <tr>
                                <td></td>
                                <td style="text-align: left">
                                     <!--  username -->
                                    <input id="username"  type=text required name=uname placeholder="Username" autofocus class="textbox"/></td>
                                 </tr>
                            <tr><td></td> </tr>
                            <tr><td></td> </tr> <tr><td></td> </tr>
                            <tr><td></td>
                                <!--password-->
                                <td style="text-align: left"><input id="password"  type=password required name=pass placeholder="Password" class="textbox"></td>
                            </tr>
                              <tr><td></td> </tr> <tr><td></td> </tr> <tr><td></td> </tr>
                            <tr>
                                <td style="text-align: left"> </td>
                                <td style="text-align: left"><input style="margin-left: 20px" type="submit" class="submit" value="Log In"/>
                                   
                                    
                                </td>
                            </tr>
                             <tr><td style="text-align: left" colspan="2"  class="linkstyle"></td> </tr>
                             <tr>
                                   <td style="text-align: left" colspan="2"  class="linkstyle">
                                    <a href="E-Crime_User_Registration.jsp" style="margin-left: 55px">Register</a>
                                </td> 
                            </tr>
                        </table>
                       <h4>
                        <%
                            if (session.getAttribute("error_login") != null)  { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("error_login")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 2800});
                    
                </script> <%
                
                session.removeAttribute("error_login");
                            }

                        %>
                        </h4>
                    </form>
                     </div>  
                       </div>
            </div>

           
<div id="footer">
<%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
               <p align="center"  style=" font-size: 17px; font-family: serif;"> &copy E-CRIME SYSTEM  <%=year%></p>
            </div>
        </div>
       
    </body>
</html>
