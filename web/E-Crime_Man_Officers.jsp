<%-- 
    Document   : E-Crime_Man_Officers
    Created on : 21-Aug-2015, 00:09:31
    Author     : user
--%>

<%@page import="ecrime_datasource.ecrime_source"%>
<%@page import="org.json.JSONObject"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/Ecrime_Icon.png"/>
        <title>E-Crime Manage Officers.</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader




function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        
    </head>
    <body onload="load_counties();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/user2menu.html" %>
<%}else if(session.getAttribute("level").equals("3")){%>
<%@include file="/menu/user1menu.html" %>
<%}else if(session.getAttribute("level").equals("4")){%>
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("10")){%>
<%@include file="/menu/Cheka_Menu.html" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%><br>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: lightsteelblue; font-size: 20px; text-align: center;">Manage Security Officers. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Man_Offic_edited") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("Man_Offic_edited")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("Man_Offic_edited");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <br/>
                    <form action="ecrime_edit_officers" method="post">
                        <br/>
                        
                        <table class="table_style" cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:20px ;">
                            <thead>
                                <tr>
                                    <th>
                                        Officer Name
                                    </th>
                                    <th>
                                        Officer ID
                                    </th>
                                    <th>
                                        Officer Tel NO
                                    </th>
                                    <th>
                                        Assignment Status
                                    </th>
                                    <th>
                                        Crime Number
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                 ecrime_source conn = new ecrime_source();
                                String Display_Land_Details = "SELECT * FROM ecrime_officers_details WHERE Officer_Status"
                                        + "!= 4";
                                conn.rs = conn.st.executeQuery(Display_Land_Details);
                                while (conn.rs.next()) {%>
                                
                                <tr>
                                   
                                    <td>
                                         
                                  <%= conn.rs.getString("FullName") %>
                                  </td>
                                  <td>
                                  <%= conn.rs.getString("Officer_ID") %>
                                  </td>
                                  <td>
                                  <%= conn.rs.getString("Officer_Tel") %>
                                  </td>
                                   <%
                                      String Status_Off = "";
                                      if (conn.rs.getInt("Officer_Status") == 0){
                                      Status_Off = "Free";
                                      }
                                      else if (conn.rs.getInt("Officer_Status") == 1){
                                      Status_Off = "Assigned";
                                      }else{
                                      Status_Off = "Suspended";
                                      }
                                      %>
                                  <td>
                                     <%= Status_Off %>
                                  </td>
                                  <%
                                  String Case_Off_No = "";
                                  if (Status_Off.equalsIgnoreCase("Assigned")){
                                  Case_Off_No = conn.rs.getString("Crime_Closure_free");
                                  }else{
                                  Case_Off_No = "None";
                                  }
                                  
                                  %>
                                  <td>
                                  <%= Case_Off_No %>
                                  </td>
                                  <td>
                                      <form action="ecrime_edit_officers" method="post">
                                          <input type="hidden" name="Officer_regID" id="Officer_regID" value="<%= conn.rs.getString("Officerme_Cri_ID") %>">
                                          <input type="submit" class="submit" value="Edit"/></form>
                                  <form action="ecrime_delete_officer" method="post">
                                          <input type="hidden" name="Officer_regID" id="Officer_regID" value="<%= conn.rs.getString("Officerme_Cri_ID") %>">
                                          <input type="hidden" name="Officer_regName" id="Officer_regName" value="<%= conn.rs.getString("FullName") %>">
                                          <input type="submit" class="submit" value="Delete"/></form>
                                  </td>
                                </tr>
                                    
                               <% }%>
       
    
    
                            </tbody>
                        </table>
                    </form>
                </div>
      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>     
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;">&copy E-CRIME SYSTEM  <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>


